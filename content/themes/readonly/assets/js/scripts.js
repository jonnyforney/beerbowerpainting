$(document).ready(function(){
  //  load post images
  $.get(
    ghost.url.api('posts')
  ).done(function (data){
    console.log('posts', data.posts);

    $.each(data.posts, function(i) {
      var post_html = "<article class='flex'>" +
                        "<div class=''>";

      //  create title
      var title = "<h4>" + this.title + "</h4>";
      if(this.meta_title) {
        title = "<h4>" + this.meta_title + "</h4>";
      }
      post_html += title;

      //  find imgs
      var imgs = $(this.html).find('img');
      console.log(imgs);
      post_html += "<div class='content flex-center'>";
      $.each(imgs, function(i) {
        console.log(this);
        var img_container = "<div class='img-container'>" +
                              "<span>" + this.alt + "</span>" +
                              this.outerHTML +
                            "</div>";

        post_html += img_container;
      });
      post_html += "</div>";

      //  create description
      var desc = "";
      if(this.meta_description) {
        desc = "<p class='flex description'>" + this.meta_description + "</p>";
      }
      post_html += desc;

      //  close tags
      post_html += "</div></article>";

      $('#projects .features').append(post_html);
    });

  }).fail(function (err){
    console.log(err);
    swal('Ops, there was an error.',err,'error');
  });

  $('#contact-form').submit(function(e) {
  	e.preventDefault();
  	$.ajax({
  		url: 'https://formspree.io/jesusdiedtosaveusfromoursins@gmail.com',
  		method: 'POST',
  		data: $(this).serialize(),
  		dataType: 'json',
  		beforeSend: function() {
  			$('#contact-form').append('<div class="alert alert--loading">Sending message…</div>');
  		},
  		success: function(data) {
        $('#contact-form .alert--loading').hide();
  			swal('Message sent!','','success');
        $('#contact-form').find("input[type=text], textarea").val("");

  		},
  		error: function(err) {
        $('#contact-form .alert--loading').hide();
  			swal('Ops, there was an error.','','error');
  		}
  	});
  });
});

function focus_el(id) {
  $('#'+id).focus();
}
